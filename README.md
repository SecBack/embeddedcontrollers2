# EmbeddedControllers2

repo for my class on embedded controllers 2

-REQUIREMENTS-
Case 2: Drivhus
I dette tilfælde skal den Embedded Controller måle lys, temperatur og luftfugtighed i
drivhuset.
    Det skal være muligt at angive, hvilken sektion/drivhus enheden er sat op i.
    Vinduerne skal åbnes, hvis det bliver for varmt. Minimum i 5 minutter ad gangen.

    Vanding
        Planterne skal vandes med jævne mellemrum. 2x 10 minutter hver dag.

    Lys
        Planterne skal have lys i 12 timer hver dag, derfor skal der tændes kunstigt lys,
        hvis lysniveauet er for lavt om dagen (overskyet)

    Testmode (simulering)
        Der skal være mulighed for at teste systemet uden at skulle vente i
        mange minutter/timer


-SOLUTION DOCUMENTATION-
Sensors
    Input
        Grove - Light Sensors
        Grove - Temperature Sensor v1.2
        Grove - Temperature&Humidity Sensor Pro v1.3

    Output
        Grove - Servo - https://wiki.seeedstudio.com/Grove-Servo/
        Grove - LED Socket Kit v1.5
        Grove - Piezo Vibration Sensor v1.1

Program Documentation

    !notice that the comments explaining the code will only be found in the header files!

    Inteded Funtionality
        - Starts by taking a greenhouse and a section
        - Depending on the temperature, open/close a window for a min of 5 min
        - watering the the plants twice at regular intervals of the day
        - Suppliment the natural light with artificial light so the plants gets a minimum 12 hours of light
        - Logging of actions and time to serial port

    Implementation/structure of program
        - Each sensor runs on their own thread
        - An action uses the instruments to perform a task, ie open a window
        - The watcher will use instruments to dertermin when to perform a task
        - Sensor information is fed to a dataCollection
        - the datacollection is used to display relevant information on the display
        - when a task is performed action a time will be sent to the serial port

        class <someAction>
            - Contains the implementation of an individual action

        Interface InputInstrument
            - Contains actions the general way to implement an input instrument

        class <someInputIntstrument>
            - contains concrete implementation of an input instrument
            - writes the datacollection since it's async
            - async
        
         Interface outputInstrument
            - Contains the general way to implement an output instrument

        class <someOutputIntstrument>
            - contains concrete implementation of an output instrument
            - takes in the amount of time the instrument will run on a thread

        class watcher
            - Takes in all input from the instruments and determines if an action should be taken
            - Keeps track of when to run what taskes
        
        struct logging
            - logging should be a datatype, with an action and timestamp

        class Display
            - uses the dataCollection for values
            - overview of statuses on left, and sensor values on the right if i have the time

        class dataCollection
            - global data object, then have the display object use that instead
            - pass the data object to the display with a pointer
            - is a singleton, meaning that there can only be one instance of this class

    Program and structure notes/ideas
        - enums med instrumenter?
        - arbjde med thread states
        - serial pc(usbtx, usbrx) read from serial port
        - maybe make a struct for the data
        - logging struct, a struct is a custom datatype

Libraies
    mbed-os 6.2.0
        Commit: a2ada74770f043aff3e61e29d164a8e78274fcd4
        Imported from: https://github.com/ARMmbed/mbed-os.git
    BSP_DISCO_F746NG tip
        Commit: 85dbcff443aa20e932494823e42c6d2a9550d17a
        Imported from: http://os.mbed.com/teams/ST/code/BSP_DISCO_F746NG/
    mbed-rtc.lib
        Commit: 1b1a0775eff0cd089351a057a8f5591055e3dc1b
        Imported from: https://github.com/rasmus0201/mbed-rtc
    mbed Servo
        Commit: 352133517cccf6614d355aa0fc75d42fb534db70
        Imported from: http://os.mbed.com/users/jdenkers/code/Servo/
    mbed-dht master
        Commit: db94a44739cda011675bf3756cc35980f9c4d3be
        Imported from: https://github.com/rasmus0201/mbed-dht