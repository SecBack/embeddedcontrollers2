#ifndef WATCHER_H
#define WATCHER_H
#include "actions.h"

class Watcher
{
    Thread lightCheckerThread;
    Thread airCheckerThread;
    Thread wateringCheckerThread;

    DataCollection *dataCollection = DataCollection::getInstance();
public:
    Watcher();

    /**
    * Checks if the conditions for turning on the artificial lights are met.
    *
    * Light sensors measure the current light, if there isn't enough light
    * artificial light will kick in. We have a required minimum amount of light that 
    * the plants need each day, if we haven't met that requirement, and there is
    * no natural light, we use artificial until the minimum amount is met.
    *
    * @param none.
    * @return none.
    */
    void lightChecker();

    /*
    * does same as lightChecker but according the the globaltimer
    */
    void lightCheckerRealTime();

    /**
    * Checks if the conditions for opning the window are met.
    *
    * while the temperature is over some number open the windows for a min of 5 min
    * if the temperture is below that number close or keep the window closed
    * and wait some time before checking the temperature again
    *
    * @param none.
    * @return none.
    */
    void airChecker();

    /**
    * Checks if the conditions for watering are met.
    *
    * two times for watering has been set through out the day, if the current time
    * the past watering time start watering
    *
    * @param none.
    * @return none.
    */
    void wateringChecker();
};
#endif