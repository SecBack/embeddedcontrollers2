#include "Watcher.h"
#include "GlobalData.h"
#include "actions.h"
#include "inputInstruments.h"
#include "outputInstruments.h"
#include "time.h"

Watcher::Watcher()
{
    // start async checkers
    lightCheckerThread.start(callback(this, &Watcher::lightChecker));
    //lightCheckerThread.start(callback(this, &Watcher::lightCheckerRealTime));

    airCheckerThread.start(callback(this, &Watcher::airChecker));
    wateringCheckerThread.start(callback(this, &Watcher::wateringChecker));
}

void Watcher::lightChecker()
{
    // executing the code should be timed, to make sure missingLightSeconds is actually a second

    // min value form light sensor that is considered being in the shadow or night  
    int minimunLight = 20000;
    
    // amount of time in a day in seconds
    int totalLightTime = 5;
    int totalTime = 10;
    int currentLightTime = 0;
    int currentTime = 0;

    LightSensor lightSensor;
    ArtificialLight artificialLight;

    while(true) {
        // no natural light
        if (dataCollection->sensorData.lightSensorValue < minimunLight) {
            // minimum light time requirement not met
            if (currentLightTime < totalLightTime) {
                // turnmon light
                artificialLight.on();
                currentLightTime++;
            }
        } else {
            // there is natural light
            artificialLight.off();
            currentLightTime++;
        }
        // if minimum require amount of light time is met
        if (currentLightTime >= totalLightTime) {
            artificialLight.off();
        }
        // a day has passed
        if (currentTime >= totalTime) {
            // reset day
            currentLightTime = 0;
            currentTime = 0;
        }
        currentTime++;
        ThisThread::sleep_for(1000ms);
    }
}

void Watcher::lightCheckerRealTime()
{
    int minLight = 20000;

    int totalLightTime = 43200;
    int totalTime = 86400;
    int currentLightTime = 0;

    LightSensor lightSensor;
    ArtificialLight artificialLight;

    while(true) {
        // is there enough natural light?
        if (dataCollection->sensorData.lightSensorValue < minLight){
            // did the plants get enough light?
            if (currentLightTime < totalLightTime) {
                // turn on light
                artificialLight.on();

                if (globalTimer.isAccelerated == true) {
                    currentLightTime += 60 * 60; // 1 hour
                } else {
                    currentLightTime++; // 1 second
                }
            }
        } else {
            // there is natural light
            artificialLight.off();
            if (globalTimer.isAccelerated == true) {
                currentLightTime += 60 *60; // 1 hour
            } else {
                currentLightTime++; // 1 second
            }
            }
            if (currentLightTime >= totalLightTime) {
                artificialLight.off();
            }
        // a day has passed
        if (globalTimer.currentDayTimeStamp >= 80000) {
            currentLightTime = 0;
        }
    }
    ThisThread::sleep_for(1000ms);
}

void Watcher::airChecker()
{
    TemperatureHumiditySensor temperatureHumiditySensor;
    FreshAir freshAir;

    while(true) {
        while(dataCollection->sensorData.temperatureValue >= 25.0) {
            freshAir.on();
            HAL_Delay(10000); // 5min = 1000 * 60 * 5 | use custom time stamps with globaltimer
        }

        freshAir.off();
        ThisThread::sleep_for(10000ms);
    }
}

void Watcher::wateringChecker()
{
    int targetDayTimeStamp1 = ((12*60)+4)*60;
    int targetDayTimeStamp2 = 16 * 60 * 60;

    Watering watering;
    Water water;

    while(true) {
        globalTimer.currentDayTimeStamp = globalTimer.dateTime % (24 * 60 * 60);

        printf("RTC: Clock is %s\n", ctime(&globalTimer.dateTime));

        if (globalTimer.currentDayTimeStamp >= targetDayTimeStamp1 &&
            globalTimer.currentDayTimeStamp <= targetDayTimeStamp2
        ) {
            if (watering.targetStatus(targetDayTimeStamp1) == true) {
                watering.run(globalTimer.currentDayTimeStamp, 10 * 60);
            } else {
                water.setState(false);
            }
        }

        if (globalTimer.currentDayTimeStamp >= targetDayTimeStamp2) {

            if (watering.targetStatus(targetDayTimeStamp2) == true) {
                watering.run(globalTimer.currentDayTimeStamp, 10 * 60);
            } else {
                water.setState(false);
            }
        }
        ThisThread::sleep_for(10000ms);
    }
}