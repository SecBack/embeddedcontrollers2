#ifndef I_OUTPUT_INSTRUMENT_H
#define I_OUTPUT_INSTRUMENT_H

/*
* This is the general way to implement an output instrument
*/
class IOutPutInstrument
{
public:
    /*
    * the constructor is generally also the same as an example below
    * ServoMotor::ServoMotor(): servoMotorPin(D8)
    * this jeg means that the instrument servoMotorPin is initialized to the pin D8
    */

    /*
    * sets the state according to the instrument
    */
    virtual void setState(bool state);

    /*
    * gets the state according to the instrument
    */
    virtual bool getState();

    /*
    * does the thing that the specific sensor does - unused might be cleaned up or changed in the final verison
    */
    virtual void run();

private:
};
#endif