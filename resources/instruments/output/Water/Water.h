#ifndef WATER_H
#define WATER_H
#include "IOutputInstrument.h"
#include "mbed.h"
#include "GlobalData.h"

class Water: public IOutPutInstrument
{
public:
    //initialize pin
    bool state;

    Water();
    void setState(bool state);
    bool getState();

    void run();

private:

    Thread thread;
};
#endif