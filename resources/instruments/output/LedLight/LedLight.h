#ifndef LED_LIGHT_H
#define LED_LIGHT_H
#include "IOutputInstrument.h"
#include "mbed.h"
#include "GlobalData.h"

/*
* this class has some leftover code, that is not being used, but not deleted yet
*/
class LedLight: public IOutPutInstrument
{
    int runTime;
    Thread thread;

public:
    DigitalOut ledLightPin;

    LedLight();
    LedLight(int runTime);

    void setState(bool state);
    bool getState();

    void timeRun();
    void run();
};
#endif