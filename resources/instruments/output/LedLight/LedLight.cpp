#include "LedLight.h"

LedLight::LedLight(): ledLightPin(D3)
{
    thread.start(callback(this, &LedLight::run));
}

LedLight::LedLight(int runTime): ledLightPin(D3)
{
    this->runTime = runTime;
    thread.start(callback(this, &LedLight::timeRun));
}

void LedLight::setState(bool state)
{
    if (state == true) {
        ledLightPin.write(1);
    } else {
        ledLightPin.write(0);
    }
}
bool LedLight::getState()
{
    return false;
}

void LedLight::timeRun()
{
    printf("run func");

    time_t startTime = time(NULL);
    time_t endTime = startTime + this->runTime;

    time_t currentTime = startTime;

    printf("starttime:   %u \n", startTime);
    printf("currentTime: %u \n", currentTime);
    printf("endTime:     %u \n", endTime);

    while(currentTime < endTime) {
        time_t currentTime = time(NULL);

        ledLightPin.write(1);

        if (currentTime < endTime) {
            printf("True \n");
        } else {
            printf("false \n");
        }

        ThisThread::sleep_for(250ms);
    }

    ledLightPin.write(0);
}

void LedLight::run()
{

}