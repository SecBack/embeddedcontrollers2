#ifndef SERVO_MOTOR_H
#define SERVO_MOTOR_H
#include "IOutputInstrument.h"
#include "mbed.h"
#include "GlobalData.h"
#include "Servo.h"

/*
* This instrument is behaving very weird if i plug it in and it moves
* all the other data that i pick up from different sensors seems to get
* randomized 
*
* remeber that you said something about it creating 'eletrical noice'
* cause im pretty sure that this code should work
*/
class ServoMotor: public IOutPutInstrument
{
public:
    Servo servoMotorPin;

    ServoMotor();
    
    void setState(bool state);
    bool getState();

    void run();


private:
    //timeOpen?
    Thread thread;
};
#endif