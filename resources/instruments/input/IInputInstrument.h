#ifndef I_INPUT_INSTRUMENT_H
#define I_INPUT_INSTRUMENT_H

/*
* This is the gerneral way to implement an input instrument
* each instance of this of this class will have a readData()
* that reads data from the sensor, depending on how each sensor
* reads data
*
* This is why there are no comments on each individual input sensor
*/
class IInputInstrument
{
public:
    /*
    * reads data according to the sensor
    */
    virtual void readData();

private:
};
#endif