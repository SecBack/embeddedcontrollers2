#include "TemperatureHumiditySensor.h"

float temperature;
float humidity;

TemperatureHumiditySensor::TemperatureHumiditySensor(): temperatureHumiditySensor(D4, DHT22)
{
    this->thread.start(callback(this, &TemperatureHumiditySensor::readData));
}

void TemperatureHumiditySensor::readData()
{
    int error;
    
    while(true) {

        error = temperatureHumiditySensor.readData();
        if (error == 0) {
            temperature =  temperatureHumiditySensor.ReadTemperature(CELCIUS);
            humidity =  temperatureHumiditySensor.ReadHumidity();

            dataCollection->sensorData.temperatureValue = temperatureHumiditySensor.ReadTemperature(CELCIUS);
            dataCollection->sensorData.humidityValue = temperatureHumiditySensor.ReadHumidity();
        }

        ThisThread::sleep_for(500ms);
    }
}