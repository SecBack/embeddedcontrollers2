#ifndef TEMPERATURE_HUMIDITY_SENSOR_H
#define TEMPERATURE_HUMIDITY_SENSOR_H
#include "IInputInstrument.h"
#include "mbed.h"
#include "GlobalData.h"
#include "DHT.h"

class TemperatureHumiditySensor: public IInputInstrument
{
    Thread thread;
    DataCollection *dataCollection = DataCollection::getInstance();

public:
    DHT temperatureHumiditySensor;

    TemperatureHumiditySensor();
    virtual void readData();
};
#endif