#include "LightSensor.h"

unsigned short lightAmount;

LightSensor::LightSensor(): lightSensorPin(A0)
{
    thread.start(callback(this, &LightSensor::readData));
}

void LightSensor::readData()
{
    while(true) {
        dataCollection->sensorData.lightSensorValue = lightSensorPin.read_u16();
        ThisThread::sleep_for(500ms);
    }
}