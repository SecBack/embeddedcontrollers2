#ifndef LIGHT_SENSOR_H
#define LIGHT_SENSOR_H
#include "IInputInstrument.h"
#include "mbed.h"
#include "GlobalData.h"

class LightSensor: public IInputInstrument
{    
    Thread thread;
    DataCollection *dataCollection = DataCollection::getInstance();

public:
    AnalogIn lightSensorPin;

    LightSensor();
    virtual void readData();
};
#endif