#include "Display.h"

Display::Display()
{
    BSP_LCD_Init();
    BSP_LCD_LayerDefaultInit(LTDC_ACTIVE_LAYER, LCD_FB_START_ADDRESS);
    BSP_LCD_SelectLayer(LTDC_ACTIVE_LAYER);

    // background color
    BSP_LCD_Clear(LCD_COLOR_BLACK);
    BSP_LCD_SetFont(&Font24);
    BSP_LCD_SetTextColor(LCD_COLOR_WHITE);
    BSP_LCD_SetBackColor(LCD_COLOR_BLACK);

    this->thread.start(callback(this, &Display::displayUpdate));
}

void Display::displayUpdate()
{
    while(true) {
        // prepare the printing of sensor values
        this->lightValueText = std::to_string(dataCollection->sensorData.lightSensorValue);
        BSP_LCD_DisplayStringAt(0, LINE(1), (uint8_t *) "Light:", LEFT_MODE);
        BSP_LCD_DisplayStringAt(0, LINE(1), (uint8_t *) this->lightValueText.c_str(), RIGHT_MODE);

        this->temperatureValueText = std::to_string((int)dataCollection->sensorData.temperatureValue);
        BSP_LCD_DisplayStringAt(0, LINE(2), (uint8_t *) "Temperature:", LEFT_MODE);
        BSP_LCD_DisplayStringAt(0, LINE(2), (uint8_t *) this->temperatureValueText.c_str(), RIGHT_MODE);

        //prepare the printing of the statuses
        if (dataCollection->actionStatus.artificialLightStatus == true) {
            this->artificialLightStatusText = std::string("On");
        } else {
            this->artificialLightStatusText = std::string("Off");
        }
        if (dataCollection->actionStatus.freshAirStatus == true) {
            this->freshAirStatusText = std::string("On");
        } else {
            this->freshAirStatusText = std::string("Off");
        }
        if (dataCollection->actionStatus.wateringStatus == true) {
            this->wateringStatusText = std::string("On");
        } else {
            this->wateringStatusText = std::string("Off");
        }

        BSP_LCD_DisplayStringAt(0, LINE(4), (uint8_t *) "Artificial Light:", LEFT_MODE);
        BSP_LCD_DisplayStringAt(0, LINE(4), (uint8_t *) this->artificialLightStatusText.c_str(), RIGHT_MODE);
        BSP_LCD_DisplayStringAt(0, LINE(5), (uint8_t *) "Fresh Air:", LEFT_MODE);
        BSP_LCD_DisplayStringAt(0, LINE(5), (uint8_t *) this->freshAirStatusText.c_str(), RIGHT_MODE);
        BSP_LCD_DisplayStringAt(0, LINE(6), (uint8_t *) "Watering:", LEFT_MODE);
        BSP_LCD_DisplayStringAt(0, LINE(6), (uint8_t *) this->wateringStatusText.c_str(), RIGHT_MODE);

        this->positionText = dataCollection->setupDataType.location;
        BSP_LCD_DisplayStringAt(0, LINE(8), (uint8_t *) "Location:", LEFT_MODE);
        BSP_LCD_DisplayStringAt(0, LINE(8), (uint8_t *) this->positionText.c_str(), RIGHT_MODE);

        this->dateTimeText = std::to_string(globalTimer.currentDayTimeStamp);
        BSP_LCD_DisplayStringAt(0, LINE(9), (uint8_t *) "Time:", LEFT_MODE);
        BSP_LCD_DisplayStringAt(0, LINE(9), (uint8_t *) this->dateTimeText.c_str(), RIGHT_MODE);

        // display update rate
        HAL_Delay(1000);

        // clearing lines instead of the whole display for speed
        BSP_LCD_ClearStringLine(1);
        BSP_LCD_ClearStringLine(2);

        BSP_LCD_ClearStringLine(4);
        BSP_LCD_ClearStringLine(5);
        BSP_LCD_ClearStringLine(6);

        BSP_LCD_ClearStringLine(8);

        BSP_LCD_ClearStringLine(9);
    }
}

void displaySetup()
{

}