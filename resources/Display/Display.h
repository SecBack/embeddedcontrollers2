#ifndef DISPLAY_H
#define DISPLAY_H
#include "mbed.h"
#include "GlobalData.h"
#include "stm32746g_discovery_lcd.h"
#include <string.h>
#include <iomanip>
#include <iostream>

class Display
{
    //variables for making the values appear on the display
    std::string lightValueText;
    std::string temperatureValueText;

    std::string artificialLightStatusText;
    std::string freshAirStatusText;
    std::string wateringStatusText;

    std::string positionText;

    std::string dateTimeText;

    Thread thread;
    DataCollection *dataCollection = DataCollection::getInstance();

public:
    /*
    * draws everything that needs to be drawn, each time the display is updated
    */
    void displayUpdate();

    /*
    * initializes the display
    */ 
    void displaySetup();

    /*
    * run the display setup
    */
    Display();
};
#endif