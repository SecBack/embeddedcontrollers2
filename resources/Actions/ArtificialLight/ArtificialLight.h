#ifndef ARTIFICIAL_LIGHT_H
#define ARTIFICIAL_LIGHT_H
#include "outputInstruments.h"
#include "Display.h"
#include "GlobalData.h"

class ArtificialLight
{
    LedLight ledLight;
    DataCollection *dataCollection = DataCollection::getInstance();

public:
    ArtificialLight();

    void run();

    /*
    * All this actions needs is to be able to turn on and off
    * ON
    */
    void on();
    /*
    * OFF
     */
    void off();
    void logging();

    ~ArtificialLight(); // calls logging
};
#endif