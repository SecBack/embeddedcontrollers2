#include "ArtificialLight.h"

ArtificialLight::ArtificialLight()
{
}

void ArtificialLight::run()
{
}

void ArtificialLight::on()
{
    ledLight.setState(1);
    dataCollection->actionStatus.artificialLightStatus = true;
}

void ArtificialLight::off()
{
    ledLight.setState(0);
    dataCollection->actionStatus.artificialLightStatus = false;
}

void ArtificialLight::logging()
{
    printf("Artificial lgiht was turned on");
}

// calls logging
ArtificialLight::~ArtificialLight()
{
    logging();
}