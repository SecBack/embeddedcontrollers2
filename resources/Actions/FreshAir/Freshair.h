#ifndef FRESH_AIR_H
#define FRESH_AIR_H
#include "outputInstruments.h"

class FreshAir
{
    bool freshAirStatus;
    time_t startTime;
    ServoMotor servoMotor;
    DataCollection *dataCollection = DataCollection::getInstance();

public:
    FreshAir();

    void run();

    /*
    * All this actions needs is to be able to turn on and off
    * ON
    */
    void on();

    /*
    * OFF
    */
    void off();
};
#endif