#include "FreshAir.h"

FreshAir::FreshAir()
{
    this->startTime = time(NULL);
}

void FreshAir::run()
{
    
}

void FreshAir::on()
{
    servoMotor.setState(true);
    dataCollection->actionStatus.freshAirStatus = true;
}

void FreshAir::off()
{
    servoMotor.setState(false);
    dataCollection->actionStatus.freshAirStatus = false;
}