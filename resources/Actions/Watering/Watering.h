#ifndef WATERING_H
#define WATERING_H
#include "outputInstruments.h"

class Watering
{
    int runTime;
    Water water;
    bool wateringStatus;
    DataCollection *dataCollection = DataCollection::getInstance();

public:
    Watering();

    /*
    * param int startTime - the time at which this action was started
    * param int runTime - The amount of time this action should be active for
    */
    void run(int startTime, int runTime);
    
    /*
    * check if there has been watered for some target time ie 18.00 has the plants been watered?
    * this check is necessary due to the time accelerated mode
    */
    bool targetStatus(int targetTime);
};
#endif