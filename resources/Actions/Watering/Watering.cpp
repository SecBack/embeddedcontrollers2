#include "Watering.h"

Watering::Watering()
{

}

void Watering::run(int startTime, int runTime)
{
    int endTime = startTime + runTime;

    if (globalTimer.currentDayTimeStamp <= endTime) {
        water.setState(true);
        dataCollection->actionStatus.wateringStatus = true;
    }
}

bool Watering::targetStatus(int targetTime)
{
    int endTime = targetTime + (10 * 60);

    if (globalTimer.currentDayTimeStamp < endTime) {
        dataCollection->actionStatus.wateringStatus = true;
        return true;
    } else {
        dataCollection->actionStatus.wateringStatus = false;
        return false;
    }
}