#include "SetupData.h"

SetupData::SetupData(): pc(USBTX, USBRX)
{
    this->pc.attach(callback(this, &SetupData::on_rx_interrupt), SerialBase::RxIrq);
}

void SetupData::on_rx_interrupt()
{
    char c;

    if (this->pc.read(&c, 1)) {
        dataCollection->setupDataType.location += c;
    }
}