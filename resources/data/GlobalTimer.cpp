#include "GlobalTimer.h"

int timeStamp;
time_t dateTime;

GlobalTimer::GlobalTimer(int interval): timer(interval) // ,true for running with no network
{
    timer.Start();
    this->timeStamp = timer.GetTimestamp();
    this->dateTime = timer.GetTime();
}

int GlobalTimer::getTimeStamp()
{
    return this->timeStamp;
}

time_t GlobalTimer::getDateTime()
{
    return this->dateTime;
}

void GlobalTimer::worker()
{
    // test time
    if (this->accelerated == true) {
        this->timeStamp += 36;
        this->dateTime = (time_t)timeStamp;

        //real time
    } else if (counter == 10) {
        this->timeStamp += 1;
        this->dateTime = (time_t)timeStamp;
        this->counter = 0;
    }
    this->counter++;
    this->currentDayTimeStamp = this->timeStamp % (24 * 60 * 60);
}

void GlobalTimer::run(bool accelerated)
{
    this->accelerated = accelerated;
    this->ticker.attach(callback(this, &GlobalTimer::worker), 10ms);
}