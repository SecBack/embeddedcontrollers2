#ifndef GLOBAL_TIMER_H
#define GLOBAL_TIMER_H
#include "Rtc.h" // online time
#include "time.h"

class GlobalTimer
{
    Bundsgaard::Rtc timer;
    Thread thread;
    Ticker ticker;
    bool accelerated;
    int counter = 0;
    int dayNum = 0;

public:
    unsigned int currentDayTimeStamp;
    unsigned int timeStamp;
    time_t dateTime;
    bool isAccelerated;

    /*
    * used to initialize the timer from the bundsgaard::RTC libray
    */
    GlobalTimer(int interval);

    /*
    * returns a timestamp
    */
    int getTimeStamp();

    /*
    * returns the timestamp as a human readable date
    */
    time_t getDateTime();

    /*
    * if accelerated time it turned on add to the time stamp each second,
    * making 24 hours pass in 24 seconds, for testing purposes
    *
    * otherwise just add one to the time stamp each second, making it the normal timestamp
    */
    void worker();

    /*
    * Run the worker function with or without accelerated time,
    * this is needed since the worcker function is special and
    * can't call any conplex functions due to losing accurcy 
    */
    void run(bool accelerated);

    //void calculatedayNum
};
#endif