#ifndef SETUP_DATA_H
#define SETUP_DATA_H
#include "GlobalData.h"
#include <string.h>
#include "mbed.h"

class SetupData
{
    UnbufferedSerial pc;
    DataCollection *dataCollection = DataCollection::getInstance();

    std::string location;

    void on_rx_interrupt();

public:
    SetupData();
};
#endif