#ifndef DATA_COLLECTION_H
#define DATA_COLLECTION_H
#include <string>

enum ActionType {
    artificialLightStatus = 1,
    freshAirStatus = 2,
    wateringStatus = 3
};
struct ActionStatus {
    bool artificialLightStatus;
    bool freshAirStatus;
    bool wateringStatus;
};
struct SensorData {
    unsigned int lightSensorValue;
    float temperatureValue;
    float humidityValue;
};
struct SetupDataType {
    std::string location;
};

/*
* this classes will be used in other classes to collect the data
* this class is a singleton meaning that there can only be one instance
*/
class DataCollection
{
    /*
    * this class contains a pointer to the instance of itself
    */
    static DataCollection* instance;

    DataCollection();

public:
    /*
    * return a pointer to the instance of this class
    */
    static DataCollection* getInstance();

    /*
    * this class has an instance of below data types
    */
    ActionStatus actionStatus;
    SensorData sensorData;
    SetupDataType setupDataType;

};
#endif